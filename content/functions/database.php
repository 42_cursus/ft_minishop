<?php

    /* database read & write secure */

    function openDatabase($name, $flag) {
        $path = $_SERVER['DOCUMENT_ROOT'] . "/".INSTALL_CSV."/" . $name . ".csv";
        $lockwait = 2;
        $waittime = 250000;

        if (!file_exists($path))
            return (null);
        if (($handle = fopen($path, $flag)) === false)
            return (null);
        $waitsum = 0;
        $locked = flock($handle, LOCK_EX | LOCK_NB);
        while(!$locked && ($waitsum <= $lockwait)) {
            $waitsum += $waittime / 1000000;
            usleep($waittime);
            $locked = flock($handle, LOCK_EX | LOCK_NB);
        }
        if(!$locked)
            return (null);
        return ($handle);
    }

    function closeDatabase(&$handle) {
        flock($handle, LOCK_UN);
        fclose($handle);
    }

    /* database globale */

    function getDatabase($name) {
        $database = array();

        if (($handle = openDatabase($name, DB_READ)) === null)
            return (null);
        $keys = fgetcsv($handle, 1000, ";");
        while (($row = fgetcsv($handle, 1000, ";")) !== false) {
            $combine = (count($keys) == count($row)) ? array_combine($keys, $row) : array("ERROR");
            $database[] = $combine;
        }
        closeDatabase($handle);
        return ($database);
    }

    /* database row */

    function setDatabaseRow($name, $datas) {
        $keys = getDatabaseKeys($name);
        $idkey = getDatabaseNameId($name);
        $nextId = getDatabaseNextId($name, $idkey);

        if (($handle = openDatabase($name, DB_ADD)) === null)
            return (false);
        $datas[$idkey] = $nextId;
        orderDatabase($datas, $keys);
        $datas = array($datas);
        var_infos($datas);
        foreach ($datas as $data)
            fputcsv($handle, encodeData($data), ";");
        closeDatabase($handle);
        return ($nextId);
    }

    function delDatabaseRowById($name, $idkey, $value) {
        $database = getDatabase($name);
        $keys = getDatabaseKeys($name);
        $keys = array_combine($keys, $keys);
        $keys = array($keys);
        $key = array_search($value, array_column($database, $idkey));
        if ($key === false)
            return (false);
        unset($database[$key]);
        $database = array_merge($keys, $database);
        if (($handle = openDatabase($name, DB_WRITE)) === null)
            return (false);
        foreach ($database as $data)
            fputcsv($handle, encodeData($data), ";");
        closeDatabase($handle);
        return (true);
    }

    function editDatabaseRowById($name, $idkey, $value, $datas) {
        $database = getDatabase($name);
        $keys = getDatabaseKeys($name);
        $keys = array_combine($keys, $keys);
        $keys = array($keys);
        $key = array_search($value, array_column($database, $idkey));
        if ($key === false)
            return (false);
        $database[$key] = array_replace_recursive($database[$key], $datas);
        $database = array_merge($keys, $database);
        if (($handle = openDatabase($name, DB_WRITE)) === null)
            return (false);
        foreach ($database as $data)
            fputcsv($handle, encodeData($data), ";");
        closeDatabase($handle);
        return (true);
    }

    function getDatabaseRowById($name, $idkey, $value) {
        if ($idkey === null || $value === null)
            return (false);
        $database = getDatabase($name);
        if ($database === null)
            return (false);
        $key = array_search($value, array_column($database, $idkey));

        if ($key !== false)
            return ($database[$key]);
        return (false);
    }

    /* database helpers */

    function getDatabaseKeys($name) {
        if (($handle = openDatabase($name, DB_READ)) === null)
            return (null);
        $keys = fgetcsv($handle, 1000, ";");
        closeDatabase($handle);
        return ($keys);
    }

    function getDatabaseNextId($name, $idkey) {
        $next = 0;

        $database = getDatabase($name);
        foreach ($database as $data) {
            if (intval($next) < intval($data[$idkey]))
                $next = $data[$idkey];
        }
        $next++;
        return ($next);
    }

    function orderDatabase(&$row, $keys){
        $row = array_merge(array_flip($keys), $row);
    }

    function getDatabaseNameId($name) {
        $keys = getDatabaseKeys($name);
        return ($keys[0]);
    }
    function encodeData($datas) {
        return (array_map('encodeUTF8', $datas));
    }

    function encodeUTF8($data) {
        str_replace(";", "", $data);
        str_replace('"', '', $data);
        return (htmlspecialchars_decode(html_entity_decode(addslashes($data), ENT_QUOTES , 'UTF-8')));
    }
?>