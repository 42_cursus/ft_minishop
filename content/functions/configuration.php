<?php
    const META_AUHTOR = "DELHOM Adrien, HANOT Adrien";
    const META_DESCRIPTION = "Super site si tu cherches un skate!";
    const META_KEYWORDS = "skate, vente";

    const DB_READ = "r";
    const DB_ADD = "a+";
    const DB_WRITE = "w";

    const DB_KEYS = [
        "articles_categories_template"  => "id;name",
        "articles_brands_template"      => "id;name",
        "articles_template"             => "id;id_category;id_brand;name;description;price;stock",
        "users_orders_template"         => "id;id_user;id_articles;date;price",
        "users_template"                => "id;email;hash_password;acces;first_name;name"
    ];

    
    const GUEST = 0;
    const USER = 1;
    const MODERATOR = 2;
    const ADMIN = 3;

    const INSTALL_CSV = "csv/";
    const COOKIE_SALT_SECRET = "c172f16bcee5df8ee574e125430c6b0c14989109fac2fe992bfdae8e63c858c6";
?>