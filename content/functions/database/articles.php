<?php

require_once $_SERVER['DOCUMENT_ROOT'] . "/content/functions/database.php";

/* articles_template */

function getAllArticles() {
    return (getDatabase("articles_template"));
}

function editOneArticle($id, $datas) {
    return (editDatabaseRowById("articles_template", "id", $id, $datas));
}

function getOneArticle($id) {
    return (getDatabaseRowById("articles_template", "id", $id));
}

function setOneArticle($datas) {
    return (setDatabaseRow("articles_template", $datas));
}

function delOneArticle($id) {
    return (delDatabaseRowById("articles_template", "id", $id));
}

/* articles_categories_template */

function getAllArticlesCategories() {
    return (getDatabase("articles_categories_template"));
}

function getOneArticlesCategories($id) {
    return (getDatabaseRowById("articles_categories_template", "id", $id));
}

function editOneArticlesCategories($id, $datas) {
    return (editDatabaseRowById("articles_categories_template", "id", $id, $datas));
}

function delOneArticlesCategories($id) {
    return (delDatabaseRowById("articles_categories_template", "id", $id));
}

function setOneArticlesCategories($datas) {
    return (setDatabaseRow("articles_categories_template", $datas));
}

/* articles_brands_template */

function getAllArticlesBrands() {
    return (getDatabase("articles_brands_template"));
}

function getOneArticlesBrands($id) {
    return (getDatabaseRowById("articles_brands_template", "id", $id));
}

function editOneArticlesBrands($id, $datas) {
    return (editDatabaseRowById("articles_brands_template", "id", $id, $datas));
}

function delOneArticlesBrands($id) {
    return (delDatabaseRowById("articles_brands_template", "id", $id));
}

function setOneArticlesBrands($datas) {
    return (setDatabaseRow("articles_brands_template", $datas));
}