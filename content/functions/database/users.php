<?php
    require_once $_SERVER['DOCUMENT_ROOT'] . "/content/functions/database.php";

    /* users_template */

    function getAllUsers() {
        return (getDatabase("users_template"));
    }

    function delOneUser($id) {
        delDatabaseRow("users_template", "id", $id);
    }

    function getOneUser($key, $value) {
        return (getDatabaseRowById("users_template", $key, $value));
    }

    function editOneUser($id, $datas) {
        return (editDatabaseRowById("users_template", "id", $id, $datas));
    }

    function setOneUser($datas) {
        return (setDatabaseRow("users_template", $datas));
    }

    /* users_purchases_template */

    function getAllUsersOrders() {
        return (getDatabase("users_orders_template"));
    }

    function getOneOrders($id) {
        return (getDatabaseRowById("users_orders_template", "id", $id));
    }

    function getOneUserOrders($id_user) {
        $purchases = array();
        foreach (getDatabase("users_orders_template") as $p)
            if (isset($p['id_user']))
                if ($p['id_user'] === $id_user)
                    $purchases[] = $p;
        if (count($purchases) == 0)
            return (false);
        return ($purchases);
    }

    function setOneUserOrders($datas) {
        return (setDatabaseRow("users_orders_template", $datas));
    }

?>