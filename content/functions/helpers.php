<?php
    require_once "database/users.php";
    require_once "database/articles.php";
    require_once "database.php";


    /*******************************
     * CHECKOUT
     */

    function checkOut() {
        $articles = getCookieCartArray();
        $id_user = $_SESSION['id'];
        $id_articles = array();
        $price = 0;
        foreach ($articles as $id => $count)
            for ($i = 0; $i < $count; $i++)
                $id_articles[] = $id;
        $id_articles = implode(",", $id_articles);
        foreach (explode(",", $id_articles) as $id)
            $price += getOneArticle($id)['price'];
        $date = date("Y-m-d H:i:s");
        $datas = compact('id_user', 'id_articles', 'date', 'price');
        return (setOneUserOrders($datas));
    }

    /*******************************
     * COOKIE CART MANAGER
     */
    function removeCookieCart() {
        setcookie('cart', 'nothing', 2147483647, '/');
    }

    function removeAllCookieCart($id_article) {
        $articles = explode("x", $_COOKIE['cart']);
        //var_infos($articles);
        foreach ($articles as $k => &$article) {
            if ($article == $id_article) {
                unset($articles[$k]);
            }
        }
        if (count($articles) == 0)
            $cart = "nothing";
        else
            $cart = implode("x", $articles);
        setcookie('cart', $cart, 2147483647, '/');
    }
    function removeOneCookieCart($id_article) {
        $articles = explode("x", $_COOKIE['cart']);
        foreach ($articles as $k => &$article) {
            if ($article == $id_article) {
                echo 'lol';
                unset($articles[$k]);
                break ;
            }
        }
        if (count($articles) == 0)
            $cart = "nothing";
        else
            $cart = implode("x", $articles);
        echo 'ok';
        setcookie('cart', $cart, 2147483647, '/');
    }
    function addOneCookieCart($id_article) {
        $exist = isset($_COOKIE['cart']) ? true : false;

        if (!$exist) {
            setcookie('cart', $id_article, 2147483647, '/');
        }
        else if ($_COOKIE['cart'] == 'nothing')
            setcookie('cart', $id_article, 2147483647, '/');
        else {
            $cart = $_COOKIE['cart'];
            $cart .= "x$id_article";
            setcookie('cart', $cart, 2147483647, '/');
        }
    }

    function getCookieCartArray() {
        $exist = isset($_COOKIE['cart']) ? true : false;
        $cart = array();
        if (!$exist)
            return (false);
        if ($_COOKIE['cart'] == 'nothing')
            return (false);
        $articles = explode("x", $_COOKIE['cart']);
        $cart = array_count_values($articles);
        return ($cart);
    }

    /*******************************
     * ARTICLES
     */
    function getShowArticles() {
        $articles = getAllArticles();
        foreach ($articles as $k => &$article) {
            if ($article['stock'] <= 0) {
                unset($articles[$k]);
                continue ;
            }
        }
        return ($articles);
    }

    /*******************************
     * USERS MANAGER
     */

    function registerUser($first_name, $name, $email, $password) {
        $datas = array(
            "email" => $email,
            "hash_password" => hash('whirlpool', $password.":".$password),
            "acces" => 1,
            "first_name" => $first_name,
            "name" => $name,
        );
        return (setOneUser($datas));
    }

    function isConnectedUser() {
        if(!isset($_SESSION['id'])) {
            $cookie = isset($_COOKIE['session']) ? $_COOKIE['session'] : false;
            if ($cookie) {
                if (!(list ($id, $hash) = explode(':', $cookie)))
                    return (false);
                if (!hash_equals(hash('whirlpool', $id . COOKIE_SALT_SECRET), $hash))
                    return (false);
                return authUserById($id);
            }
        }
        else
            return (true);
    }

    function authUser($email, $password) {
        if (!($user = getOneUser("email", $email)))
            errorRedirect("This account doesn't exist!", "/login");
        if ($user['hash_password'] != hash('whirlpool', $password.":".$password))
            errorRedirect("This account doesn't exist!", "/login");
        $cookie = $user['id'];
        $hash = hash('whirlpool', $user['id'] . COOKIE_SALT_SECRET); // cookie => id:HASH
        $cookie .= ':'.$hash;
        setcookie('session', $cookie, 2147483647, '/');
        $_SESSION['id'] = $user['id'];
        $_SESSION['acces'] = $user['acces'];
        succesRedirect("You're now connected!", "/");
    }

    function authUserById($id) {
        if (!($user = getOneUser("id", $id)))
            return (false);
        $_SESSION['id'] = $id;
        $_SESSION['acces'] = $user['acces'];
        return (true);
    }


    /*******************************
     * REDIRECT HELPERS
     */
    function errorRedirect($error, $route) {
        $_SESSION['error'] = $error;
        redirect($route);
    }

    function succesRedirect($succes, $route) {
        $_SESSION['succes'] = $succes;
        redirect($route);
    }

    function redirect($route) {
        switch($route) {
            case 404:
                header('Location: /404');
                break;
            case 403:
                header('Location: /403');
                break;
            default:
                header('Location: '. $route);
                break;
        }
        exit();
    }
?>