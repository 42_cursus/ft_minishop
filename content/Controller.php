<?php
    require_once "functions/configuration.php";
    require_once "functions/database.php";
    require_once "functions/routes.php";
    require_once "functions/user_session.php";

    require_once "functions/helpers.php";
    require_once "functions/database/users.php";
    require_once "functions/database/articles.php";


    function var_infos($var) {
        /*echo '<pre>' . var_export($var, true) . '</pre>';*/
    }



    // WEBSITE ROUTES
    addRoute("/",                       "home",                 GUEST,      false);
    addRoute("/home",                   "home",                 GUEST,      false);
    addRoute("/register",               "register",             GUEST,      true);
    addRoute("/login",                  "login",                GUEST,      true);
    addRoute("/article/*",              "article",              GUEST,      false);
    addRoute("/cart",                   "cart",                 GUEST,      false);
    addRoute("/checkout",               "checkout",             GUEST,      false); //exception gérer en live
    addRoute("/404",                    "404",                  GUEST,      false);
    addRoute("/403",                    "403",                  GUEST,      false);
    addRoute("/orders",                 "orders",               USER,       false);
    addRoute("/order/*",                "order",                USER,       false);
    addRoute("/logout",                 "",                     USER,       false, "logout");
    
// ADMIN ROUTES
    addRoute("/admin",                  "admin/home",                 MODERATOR,  false);
    addRoute("/admin/home",             "admin/home",                 MODERATOR,  false);
   
    addRoute("/admin/manager",          "admin/manager",              MODERATOR,  false);

    addRoute("/admin/orders",           "admin/orders",               MODERATOR,  false);
    addRoute("/admin/order/*",          "admin/order",                MODERATOR,  false);

    addRoute("/admin/articles",         "admin/articles",               MODERATOR,  false);
    addRoute("/admin/article/*",        "admin/article",                MODERATOR,  false);
    addRoute("/admin/article/add",      "admin/article/add",            MODERATOR,  false);
    addRoute("/admin/article/remove/*", "admin/article/remove",         MODERATOR,  false);

    addRoute("/admin/forms/article/add",      "",          MODERATOR,  false, "admin/forms/article/add");
    addRoute("/admin/forms/article/edit",     "",          MODERATOR,   false, "admin/forms/article/edit");


    addRoute("/admin/users",                "admin/users",                      MODERATOR,  false);
    addRoute("/admin/user/*",               "admin/user",                       MODERATOR,  false);
    addRoute("/admin/forms/user/edit",      "",                                 MODERATOR,  false, "admin/forms/user/edit");

    // DEV ROUTES
    addRoute("/forms/register",     "",                           GUEST,       false, "forms/register");
    addRoute("/forms/login",        "",                           GUEST,       false, "forms/login");
    addRoute("/cart/add/*",         "cart/add",                   GUEST,       false);
    addRoute("/cart/remove/*",      "cart/remove",                GUEST,       false);
    addRoute("/cart/removeAll/*",   "cart/removeAll",             GUEST,       false);


session_start();

    if (!isset($_SESSION['acces']))
        $_SESSION['acces'] = 0;
    $routeKey = getRouteKey();
    $route = getRoute($routeKey);
    $view = "view.php";
    if (!empty($route['worker']))
        require_once "worker/" . $route['worker'] . ".php";
    ob_start();
    if (!empty($route['view']))
        require_once "view/". $view;
    ob_end_flush();
?>
