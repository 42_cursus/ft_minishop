<?php
    session_destroy();
    unset($_COOKIE['session']);
    setcookie('session', null, -1, '/');
    redirect('/');
?>