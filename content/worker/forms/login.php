<?php
$datas = ["l-email", "l-password", "l-submit"];
$isset = true;

foreach ($datas as $d)
    if (!isset($_POST[$d]))
        $isset = false;
if ($isset) {
    if ($_POST["l-submit"] === "Log in")
    {
        if (empty($_POST["l-email"]))
            errorRedirect("Empty email!", "/login");
        if (!filter_var($_POST["l-email"], FILTER_VALIDATE_EMAIL))
            errorRedirect("Not valide email!", "/login");

        if (empty($_POST["l-password"]))
            errorRedirect("Empty password!", "/login");
        if (!ctype_alnum($_POST["l-password"]))
            errorRedirect("Not valide password!", "/register");
        
        if (authUser($_POST["l-email"], $_POST["l-password"]) !== false) {
            succesRedirect("Congratulations ! ", "/");
        }
    }
}
else
    redirect("/login");

?>
