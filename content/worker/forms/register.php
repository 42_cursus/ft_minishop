<?php
    $datas = ["r-first-name", "r-name", "r-email", "r-email-confirm", "r-password", "r-password-confirm", "r-submit"];
    $isset = true;
    var_infos($_POST);
    foreach ($datas as $d)
        if (!isset($_POST[$d]))
            $isset = false;
    if ($isset) {
        if ($_POST["r-submit"] === "Register")
        {
            //firstname
            if (empty($_POST['r-first-name']))
                errorRedirect("Empty first name!", "/register");
            if (!ctype_alpha($_POST["r-first-name"]))
                errorRedirect("Not valid first name format!", "/register");
            if (!(strlen($_POST["r-first-name"]) >= 2 && strlen($_POST["r-first-name"]) <= 24))
                errorRedirect("Not valid first name format!", "/register");
            //name
            if (empty($_POST['r-name']))
                errorRedirect("Empty name!", "/register");
            if (!ctype_alpha($_POST["r-name"]))
                errorRedirect("Not valid name format!", "/register");
            if (!(strlen($_POST["r-name"]) >= 2 && strlen($_POST["r-name"]) <= 24))
                errorRedirect("Not valid name format!", "/register");
            //email
            if (empty($_POST["r-email"]) || empty($_POST["r-email-confirm"]))
                errorRedirect("Empty email!", "/register");
            if (strcmp($_POST["r-email"], $_POST["r-email-confirm"]) != 0)
                errorRedirect("Not same email!", "/register");
            if (!filter_var($_POST["r-email"], FILTER_VALIDATE_EMAIL))
                errorRedirect("Not valide email!", "/register");
            if (getOneUser("email", $_POST['r-email']))
                errorRedirect("Email already used!", "/register");
            //password
            if (empty($_POST["r-password"]) || empty($_POST["r-password-confirm"]))
                errorRedirect("Empty password!", "/register");
            if (strcmp($_POST["r-password"], $_POST["r-password-confirm"]) != 0)
                errorRedirect("Not same password!", "/register");
            if (!ctype_alnum($_POST["r-password"]))
                errorRedirect("Not valide password!", "/register");
            if (registerUser($_POST['r-first-name'], $_POST['r-name'], $_POST["r-email"], $_POST["r-password"]) !== false) {
                succesRedirect("Congratulations ! ", "/");
            }
        }
    }
    else
        redirect("/register");

?>