<?php
    header("Content-Type: text/html; charset=utf-8");
    $datas = ["user-id", "user-acces", "user-submit"];
    $isset = true;
    var_infos($_POST);
    foreach ($datas as $d)
        if (!isset($_POST[$d]))
            $isset = false;
    if ($isset) {
        if ($_POST["user-submit"] === "Modify")
        {
            $datas = array(
                "acces"   => $_POST["user-acces"],
            );
            editOneUser($_POST['user-id'], $datas);
            succesRedirect("User # " .$_POST['user-id']. " modified", "/admin/users");
        }
    }
    else
        redirect("/admin/users");
    ?>
?>
