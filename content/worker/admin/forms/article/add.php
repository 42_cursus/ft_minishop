<?php
header("Content-Type: text/html; charset=utf-8");

$isset = true;
$datas = ["article-name", "article-description", "article-category", "article-brand", "article-price"];

foreach ($datas as $d)
    if (!isset($_POST[$d]))
        $isset = false;
if ($isset) {
    if ($_POST["article-submit"] === "Add")
    {
        $datas = array(
            "name"          => $_POST["article-name"],
            "description"   => $_POST["article-description"],
            "id_category"      => $_POST["article-category"],
            "id_brand"         => $_POST["article-brand"],
            "price"         => $_POST["article-price"],
        );
        setOneArticle($datas);
        succesRedirect("Article # " .$_POST['article-id']. " added", "/admin/articles");
    }
}
else
    redirect("/admin/articles");

?>
