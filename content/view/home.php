<section class="articles">
    Brands   <select id="brands" onchange="filterArticles();">
        <option value="0" selected>None</option>
    <?php foreach (getAllArticlesBrands() as $brand) { ?>
        <option value="<?php echo $brand['id']?>"><?php echo $brand['name']?></option>
    <?php } ?>
    </select>
    Categories   <select id="categories" onchange="filterArticles();">
        <option value="0" selected>None</option>
        <?php foreach (getAllArticlesCategories() as $category) { ?>
            <option value="<?php echo $category['id']?>"><?php echo $category['name']?></option>
        <?php } ?>
    </select>
</section>
<section class="articles">
   <?php
   $articles = getShowArticles();
   if (count($articles) == 0)
       echo "No articles.";
   else
   foreach ($articles as $a) { ?>
        <div class="article">
            <div class="article_info">
                <img height="200" class="article_image" src="/public/images/categories/<?php echo $a['id_category'] . ".jpg"; ?>" alt="" />
                <h3 class="article_title"><?php echo $a['name']; ?></h3>
                <span class="article_year extra highlight"><span class="id-category"><?php echo getOneArticlesCategories($a['id_category'])['name']; ?></span> ( <span class="id-brand"><?php echo getOneArticlesBrands($a['id_brand'])['name']; ?></span> )</span>
                <span class="article_region extra highlight"><p><?php echo substr($a['description'], 0, 200);?>[...]</p></span>
                <span class="article_price highlight"><?php echo $a['price']; ?> $</span>
                <a href="/cart/add/<?php echo $a['id']; ?>"><button class="action article-button article-buy">Add to cart</button></a><a href="/article/<?php echo $a['id'] ?>"><button class="action article-button article-infos">Informations</button></a>
            </div>
        </div>
    <?php } ?>
</section>
<script language="javascript">
    var brands = document.getElementById("brands");
    var categories = document.getElementById("categories");

    function findAncestor (el, cls) {
        while ((el = el.parentElement) && !el.classList.contains(cls));
        return el;
    }

    function filterArticles() {
        var brand = brands.options[brands.selectedIndex].innerHTML;
        var category = categories.options[categories.selectedIndex].innerHTML;

        var id_brand = document.getElementsByClassName('id-brand');
        var id_category = document.getElementsByClassName('id-category');
        Array.prototype.filter.call(id_brand, function(el){
            findAncestor(el, "article").style.display = 'inline-block';
        });
        Array.prototype.filter.call(id_category, function(el){
            findAncestor(el, "article").style.display = 'inline-block';
        });

        Array.prototype.filter.call(id_brand, function(el){
            if (brand != el.innerHTML)
                if (brand != "None")
                    findAncestor(el, "article").style.display = 'none';
        });
        Array.prototype.filter.call(id_category, function(el){
            if (category != el.innerHTML)
                if (category != "None")
                    findAncestor(el, "article").style.display = 'none';
        });
    }
</script>