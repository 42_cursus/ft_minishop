<?php
    $carts = getCookieCartArray();
    $total = 0;
?>

<div class="main">
    <h1>Shopping cart</h1>

    <section class="shopping-cart">
        <?php if ($carts !== false) { ?>
            <ol class="ui-list shopping-cart--list" id="shopping-cart--list">
                <?php foreach ($carts as $id => $cart) {
                    $article = getOneArticle($id);
                    $total += (doubleval($article['price']) * $cart);
                    ?>
                    <li style="cursor:pointer" onclick="window.location='/article/<?php echo $article['id'] ?>'" class="_grid shopping-cart--list-item">
                        <div class="_column product-image" style="background-image: url('/public/images/categories/<?php echo $article['id_category'] .".jpg";?>');">
                        </div>
                        <div class="_column product-info">
                            <h4 class="product-name"><?php echo $article['name'];?></h4>
                            <p class="product-desc"><?php echo substr($article['description'], 0, 100);?>[...]</p>
                            <div class="price product-single-price"><?php echo $article['price'];?></div>
                        </div>
                        <div class="_column product-modifiers">
                            <div class="_grid">
                                <a href="/cart/remove/<?php echo $id; ?>"><button class="_btn _column product-subtract">−</button></a>
                                <div class="_column product-qty"><?php echo $cart;?></div>
                                <a href="/cart/add/<?php echo $id; ?>"><button class="_btn _column product-plus">+</button></a>
                            </div>
                            <a href="/cart/removeAll/<?php echo $id; ?>"><button class="_btn entypo-trash product-remove">Remove</button></a>
                            <div class="price product-total-price">$<?php echo $cart*intval($article['price']);?></div>
                        </div>
                    </li>
                <?php } ?>
            </ol>
            <div class="_grid cart-totals">
                <div class="_column total" id="totalCtr">
                    <div class="cart-totals-key">Total</div>
                    <div class="cart-totals-value">$<?php echo doubleval($total);?></div>
                </div>
                <div class="_column checkout">
                    <a href="/checkout"><button class="_btn checkout-btn entypo-forward">Checkout</button></a>
                </div>
            </div>
        <?php }
        else { ?>
        <p class="text-center">Empty cart.</p>
        <?php } ?>


    </section>
</div>
