<?php
    if (!isConnectedUser())
        errorRedirect("You need to be connected to checkout! <b><a href='/login'>Log in here</a></b>", "/cart");
    else {
        removeCookieCart();
        checkOut();
        succesRedirect("Congratulations! You can see <b><a href='/orders'>your orders here</a></b>", "/cart");
    }
?>
