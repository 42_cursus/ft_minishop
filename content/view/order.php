<?php
$id = $_GET[2];
if (!($order = getOneOrders($id)))
    redirect(404);
if ($order['id_user'] != $_SESSION['id'])
    redirect(403);
$articles = explode(",", $order['id_articles']);
$articles = array_count_values($articles);
$total = 0;
?>

<div class="main">
    <h1>Order #<?php echo $id ?></h1>

    <section class="shopping-cart">
            <ol class="ui-list shopping-cart--list" id="shopping-cart--list">
                <?php foreach ($articles as $id => $count) {
                    $article = getOneArticle($id);
                    $total += (doubleval($article['price']) * $count);
                    ?>
                    <li style="cursor:pointer" onclick="window.location='/article/<?php echo $article['id'] ?>'" class="_grid shopping-cart--list-item">
                        <div class="_column product-image" style="background-image: url('/public/images/categories/<?php echo $article['id_category'] .".jpg";?>');">
                        </div>
                        <div class="_column product-info">
                            <h4 class="product-name"><?php echo $article['name'];?></h4>
                            <p class="product-desc"><?php echo substr($article['description'], 0, 100);?>[...]</p>
                            <div class="price product-single-price"><?php echo $article['price'];?></div>
                        </div>
                        <div class="_column product-modifiers">
                            <div class="text-center _grid">
                                <div class="_column product-qty"><?php echo $count;?></div>
                            </div>
                            <div class="price product-total-price">$<?php echo $count*intval($article['price']);?></div>
                        </div>
                    </li>
                <?php } ?>
            </ol>
            <div class="_grid cart-totals">
                <div class="_column total" id="totalCtr">
                    <div class="cart-totals-key">Total</div>
                    <div class="cart-totals-value">$<?php echo doubleval($total);?></div>
                </div>
                <div class="_column checkout">
                    <a href="/checkout"><button class="_btn checkout-btn entypo-forward">Checkout</button></a>
                </div>
            </div>
    </section>
</div>
