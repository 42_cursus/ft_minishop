<?php
    $orders = getOneUserOrders($_SESSION['id']);
?>

<div class="main">
    <h1>Orders commands</h1>

    <section class="shopping-cart">
        <?php if ($orders !== false) { ?>
            <ul class="ui-list shopping-cart--list" id="shopping-cart--list">
                <?php foreach ($orders as $id => $order) { ?>
                    <li>
                        <a style="color: #7e00ff" href="/order/<?php echo $order['id'] ?>">#<?php echo $order['id'] ?> | <?php echo $order['date'] ?> | <?php echo $order['price'] ?> $</a>
                    </li>
                <?php } ?>
            </ul>
        <?php }
        else { ?>
            <p class="text-center">No orders.</p>
        <?php } ?>


    </section>
</div>
