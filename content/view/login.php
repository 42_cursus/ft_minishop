<div class="form_template" style="margin: 40px auto;">
    <h1>Log in<span> Not member yet ? <a href="/register" style="font-weight: bold">Register here</a></span></h1>
    <form action="/forms/login" method="POST">
        <div class="section">> Connexion informations</div>
        <div class="inner-wrap">
            <label>Email <input type="email" name="l-email" /></label>
            <label>Password <input type="password" name="l-password" /></label>
        </div>
        <div class="button-section">
            <input type="submit" class="action article-button article-buy" name="l-submit" value="Log in">
        </div>
    </form>
</div>