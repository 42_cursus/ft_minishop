<!DOCTYPE html>
<html>
<head>
    <title>Minishop | Wine market</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="author" content="<?php echo META_AUHTOR; ?>"/>
    <meta name="description" content="<?php echo META_DESCRIPTION; ?>"/>
    <meta name="keywords" content="<?php echo META_KEYWORDS; ?>"/>
    <link rel="stylesheet" type="text/css" href="/public/css/minishop.css">
</head>

<body>
<?php if (userAlert('error')) { ?>
<div class="alert error">
    <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
    <strong>Error !</strong> <?php echo getUserAlert('error'); ?>
</div>
<?php } ?>

<?php if (userAlert('succes')) { ?>
    <div class="alert succes">
        <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
        <strong>Succes !</strong> <?php echo getUserAlert('succes'); ?>
    </div>
<?php } ?>

<div align="center">
    <a href="/"><img src="/public/images/logo.jpg"></a>
</div>


<div id="menu">
    <ul>
        <li><a href="/">Home</a></li>
        <?php if (!isConnectedUser()) { ?>
        <li><a href="/register">Register</a></li>
        <li><a href="/login">Log in</a></li>
        <?php }
        else {?>
        <li><a href="/orders">Orders</a></li>
        <?php } ?>
        <li><a href="/cart">Cart</a></li>
        <?php if ($_SESSION['acces'] >= MODERATOR) { ?><li><a class="button-admin" href="/admin">Admin</a></li> <?php } ?>
        <?php if (isConnectedUser()) { ?><li><a class="button-logout" href="/logout">Log out</a></li> <?php } ?>
    </ul>
</div>


<div id="content" <?php if ($route['route'] == "/register") {?> style="background: url('/public/images/register_wine.jpg') no-repeat top center" <?php } ?>>
    <?php require_once $route['view']; ?>

</div>

<div id="footer">
    <p class="text-center">Copyright &copy; Minishop - 2017</p>
</div>
</body>

</html>

