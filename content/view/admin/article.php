<?php
header("Content-Type: text/html; charset=utf-8");
$id = $_GET[3];

if (!($article = getOneArticle($id)))
    redirect(404);
?>

<div class="container">
    <div align="center" class="left-column">
        <img src="/public/images/categories/<?php echo $article['id_category'] . ".jpg"; ?>" alt="">
    </div>
    <div class="right-column">
        <form action="/admin/forms/article/edit" method="POST" accept-charset="UTF-8">
            <input name="article-id" type="hidden" value="<?php echo $article['id'] ?>">
            <div class="product-description">
                <div>Name : <input type="text" name="article-name" value="<?php echo $article['name'] ?>"></div>
                <div>Category : <select name="article-category">
                                    <?php foreach (getAllArticlesCategories() as $category) { ?>
                                        <option <?php if (getOneArticle($id)['id_category'] === $category['id']) echo "selected "; ?>value="<?php echo $category['id'] ?>"> <?php echo $category['name'] ?></option>
                                    <?php } ?>
                                </select>
                </div>
                <div>Brand : <select name="article-brand">
                                <?php foreach (getAllArticlesBrands() as $brand) { ?>
                                    <option <?php if (getOneArticle($id)['id_brand'] === $brand['id']) echo "selected "; ?> value="<?php echo $brand['id'] ?>"> <?php echo $brand['name'] ?></option>
                                <?php } ?>
                            </select>
                </div>
                <div>Description : <br><textarea name="article-description" rows="10" cols="100" style="max-width: 600px;"><?php echo $article['description'] ?></textarea></div>
            </div>
            <div class="product-price">
                <span><input name="article-price" type="number" step="0.01" min="0" value="<?php echo $article['price'] ?>" style="max-width: 110px;"> $</span>
                <input class="cart-btn" type="submit" name="article-submit" value="Modify">
            </div>
        </form>
    </div>
</div>