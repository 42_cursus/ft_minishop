<?php
$articles = getShowArticles();
?>

<div class="text-center main">
    <h1>Articles manager</h1>
    <a href="/admin/article/add"><button align="center" class="admin-button" style="font-size: 40px;">Add an article</button></a>
    <?php if (count($articles) > 0) { ?>
    <section class="shopping-cart">
    <ol class="ui-list shopping-cart--list" id="shopping-cart--list">
        <?php foreach ($articles as $article) { ?>
            <li style="cursor:pointer"  class="_grid shopping-cart--list-item">
                <div class="_column product-image" style="background-image: url('/public/images/categories/<?php echo $article['id_category'] .".jpg";?>');">
                </div>
                <div class="_column product-info">
                    <h4 onclick="window.location='/admin/article/<?php echo $article['id'] ?>'" class="product-name"><?php echo $article['name'];?></h4>
                    <p class="product-desc"><?php echo substr($article['description'], 0, 100);?>[...]</p>
                    <div class="price ">STOCK : <?php echo $article['stock'];?></div>
                </div>
                <div style="z-index:10000" class="_column product-modifiers">
                    <a href="/admin/article/remove/<?php echo $article['id']; ?>"><button class="_btn entypo-trash product-remove">Remove</button></a>
                    <div class="price product-total-price">$<?php echo $article['price'];?></div>
                </div>
            </li>
        <?php } ?>
    </ol>
<?php }
else { ?>
    <p class="text-center">No articles</p>
<?php } ?>
    </section>
    </div>
