<?php
    $user = getOneUser("id", $_GET[3]);
?>
<div style="margin: 50px" align="center">
    <form action="/admin/forms/user/edit" method="POST">
        Acces user <?php echo $user['id'] ?> (<?php echo $user['email'] ?>) :
        <input type="hidden" name="user-id" value="<?php echo $user['id'] ?>">
        <select name="user-acces">
            <option value="1">User</option>
            <option value="2">Moderator</option>
            <option value="3">Admin</option>
        </select>
        <input type="submit" name="user-submit" value="Modify">
    </form>
</div>