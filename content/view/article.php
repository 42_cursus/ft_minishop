<?php
    $id = $_GET[2];
    if (!($article = getOneArticle($id)))
        redirect(404);
?>

<div class="container">
    <div align="center" class="left-column">
        <img src="/public/images/categories/<?php echo $article['id_category'] . ".jpg"; ?>" alt="">
    </div>
    <div class="right-column">
        <div class="product-description">
            <span><?php echo getOneArticlesCategories($article['id_category'])['name'] ?> ( <?php echo getOneArticlesBrands($article['id_brand'])['name'] ?> )</span>
            <h1><?php echo $article['name'] ?></h1>
            <p><?php echo $article['description'] ?></p>
        </div>

        <div class="product-price">
            <span><?php echo $article['price'] ?> $</span>
            <a href="/cart/add/<?php echo $article['id'] ?>" class="cart-btn">Add to cart</a>
        </div>
    </div>
</div>