<div class="form_template">
    <h1>Register now! <span>Sign up and tell us what you think of the site!</span></h1>
    <form action="/forms/register" method="POST">
        <div class="section"><span>1</span>About you</div>
        <div class="inner-wrap">
            <label>First Name <input type="text" name="r-first-name" /></label>
            <label>Name <input type="text" name="r-name" /></label>
        </div>

        <div class="section"><span>2</span>Email</div>
        <div class="inner-wrap">
            <label>Email Address <input type="email" name="r-email" /></label>
            <label>Confirm Address <input type="email" name="r-email-confirm" /></label>
        </div>

        <div class="section"><span>3</span>Passwords</div>
        <div class="inner-wrap">
            <label>Password <input type="password" name="r-password" /></label>
            <label>Confirm Password <input type="password" name="r-password-confirm" /></label>
        </div>
        <div class="button-section text-center">
            <input type="submit" class="action article-button article-buy" name="r-submit" value="Register">
        </div>
    </form>
</div>