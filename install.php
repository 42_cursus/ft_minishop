<?php
    require_once "content/functions/database.php";
    require_once "content/functions/configuration.php";

    echo "<ul>";
    if (!file_exists(INSTALL_CSV)) {
        mkdir(INSTALL_CSV);
        echo "<li>INSTALL_CSV created at root.</li>";
    }

    foreach (DB_KEYS as $file => $key) {
        if (!file_exists(INSTALL_CSV . $file . ".csv")) {
            echo "<li>" . $file . ".csv created in directory /" . INSTALL_CSV ."</li>";
            file_put_contents(INSTALL_CSV . $file . ".csv", $key. "\n", FILE_APPEND);
        }
    }
    echo "</ul>";


?>